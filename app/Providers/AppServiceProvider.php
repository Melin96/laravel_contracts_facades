<?php

namespace App\Providers;

use App;
use App\Services\DocumentParsers\DocumentParserService;
use App\Services\DocumentParsers\ParserList;
use App\Services\DocumentParsers\ParserListWithDocumentParsers;
use App\Services\Menu\Contracts\MenuListFacade as MenuListFacadeInterface;
use App\Services\Menu\Contracts\MenuService as MenuServiceInterface;
use App\Services\Menu\MenuListFacade;
use App\Services\Menu\MenuService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Schema;
use Validator;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('block.mobile_submenu', function ($view) {
            $view->with('menuItems', $this->app->make(MenuListFacade::class));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MenuListFacadeInterface::class, MenuListFacade::class);
        $this->app->singleton(MenuServiceInterface::class, MenuService::class);

        $this->registerFileParser();
    }

    /**
     * Registers classes to parse files. It is used by calculators and assemblies.
     *
     * @return void
     */
    public function registerFileParser(): void
    {
        $this->app->bind(ParserList::class, ParserListWithDocumentParsers::class);
        $this->app->bind(DocumentParserService::class, function ($app) {
            return new DocumentParserService($app->make(ParserList::class));
        });
    }
}
