<?php

namespace App\Services\Menu;

use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuListFacade as MenuListFacadeInterface;
use App\Services\Menu\Contracts\MenuService as MenuServiceInterface;

class MenuService implements MenuServiceInterface, MenuListFacadeInterface
{
    /**
     * @var MenuListFacadeInterface
     */
    private $menuFacade;

    public function __construct(MenuListFacadeInterface $menuFacade)
    {
        $this->menuFacade = $menuFacade;
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->add($this->getLeads()->getMenu());
        $menu->add($this->getRequests()->getMenu());
        $menu->add($this->getOrders()->getMenu());
        $menu->add($this->getPartners()->getMenu());
        $menu->add($this->getTasks()->getMenu());
        $menu->add($this->getCashBox()->getMenu());
        $menu->add($this->getPayments()->getMenu());
        $menu->add($this->getLogistics()->getMenu());
        $menu->add($this->getBaseInfo()->getMenu());
        $menu->add($this->getSettings()->getMenu());

        return $menu;
    }

    /**
     * @return MenuItem
     */
    public function getLeads(): MenuItem
    {
        return $this->menuFacade->getLeads();
    }

    /**
     * @return MenuItem
     */
    public function getRequests(): MenuItem
    {
        return $this->menuFacade->getRequests();
    }

    /**
     * @return MenuItem
     */
    public function getOrders(): MenuItem
    {
        return $this->menuFacade->getOrders();
    }

    /**
     * @return MenuItem
     */
    public function getPartners(): MenuItem
    {
        return $this->menuFacade->getPartners();
    }

    /**
     * @return MenuItem
     */
    public function getTasks(): MenuItem
    {
        return $this->menuFacade->getTasks();
    }

    /**
     * @return MenuItem
     */
    public function getCashBox(): MenuItem
    {
        return $this->menuFacade->getCashBox();
    }

    /**
     * @return MenuItem
     */
    public function getPayments(): MenuItem
    {
        return $this->menuFacade->getPayments();
    }

    /**
     * @return MenuItem
     */
    public function getLogistics(): MenuItem
    {
        return $this->menuFacade->getLogistics();
    }

    /**
     * @return MenuItem
     */
    public function getBaseInfo(): MenuItem
    {
        return $this->menuFacade->getBaseInfo();
    }

    /**
     * @return MenuItem
     */
    public function getSettings(): MenuItem
    {
        return $this->menuFacade->getSettings();
    }
}
