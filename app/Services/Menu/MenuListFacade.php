<?php

namespace App\Services\Menu;


use App\Services\FilterService;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\MenuItems\BaseInfoItem;
use App\Services\Menu\MenuItems\CashBoxItem;
use App\Services\Menu\MenuItems\LeadItem;
use App\Services\Menu\MenuItems\LogisticItem;
use App\Services\Menu\MenuItems\OrderItem;
use App\Services\Menu\MenuItems\PartnerItem;
use App\Services\Menu\MenuItems\PaymentItem;
use App\Services\Menu\MenuItems\RequestItem;
use App\Services\Menu\MenuItems\SettingsItem;
use App\Services\Menu\MenuItems\TaskItem;
use App\Services\Menu\Contracts\MenuListFacade as MenuListFacadeInterface;


class MenuListFacade implements MenuListFacadeInterface
{
    /**
     * Menu list
     * @var MenuItem
     */
    private $leads;

    /**
     * @var MenuItem
     */
    private $orders;

    /**
     * @var MenuItem
     */
    private $partners;

    /**
     * @var MenuItem
     */
    private $requests;

    /**
     * @var MenuItem
     */
    private $tasks;

    /**
     * @var MenuItem
     */
    private $logistics;

    /**
     * @var MenuItem
     */
    private $cashbox;

    /**
     * @var MenuItem
     */
    private $payments;

    /**
     * @var MenuItem
     */
    private $settings;

    /**
     * @var MenuItem
     */
    private $baseInfo;

    /**
     * @var FilterService
     */
    private $filterService;

    public function __construct(FilterService $filterService)
    {
        $this->filterService = $filterService;
    }

    /**
     * @return MenuItem
     */
    public function getLeads() : MenuItem
    {
        if (!isset($this->leads)) {
            $this->leads = new LeadItem($this->filterService);
        }

        return $this->leads;
    }

    /**
     * @return MenuItem
     */
    public function getRequests(): MenuItem
    {
        if (!isset($this->requests)) {
            $this->requests = new RequestItem($this->filterService);
        }

        return $this->requests;
    }

    /**
     * @return MenuItem
     */
    public function getOrders(): MenuItem
    {
        if (!isset($this->orders)) {
            $this->orders = new OrderItem($this->filterService);
        }


        return $this->orders;
    }

    /**
     * @return MenuItem
     */
    public function getPartners(): MenuItem
    {
        if (!isset($this->partners)) {
            $this->partners = new PartnerItem();
        }

        return $this->partners;
    }

    /**
     * @return MenuItem
     */
    public function getTasks(): MenuItem
    {
        if (!isset($this->tasks)) {
            $this->tasks = new TaskItem();
        }

        return $this->tasks;
    }

    /**
     * @return MenuItem
     */
    public function getCashBox(): MenuItem
    {
        if (!isset($this->cashbox)) {
            $this->cashbox = new CashBoxItem();
        }

        return $this->cashbox;
    }

    /**
     * @return MenuItem
     */
    public function getPayments(): MenuItem
    {
        if (!isset($this->payments)) {
            $this->payments = new PaymentItem();
        }

        return $this->payments;
    }

    /**
     * @return MenuItem
     */
    public function getLogistics(): MenuItem
    {
        if (!isset($this->logistics)) {
            $this->logistics = new LogisticItem();
        }

        return $this->logistics;
    }

    /**
     * @return MenuItem
     */
    public function getBaseInfo(): MenuItem
    {
        if (!isset($this->baseInfo)) {
            $this->baseInfo = new BaseInfoItem($this->filterService);
        }

        return $this->baseInfo;
    }

    /**
     * @return MenuItem
     */
    public function getSettings(): MenuItem
    {
        if (!isset($this->settings)) {
            $this->settings = new SettingsItem();
        }

        return $this->settings;
    }
}
