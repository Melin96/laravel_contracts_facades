<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Model\Task\TaskProject;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class TaskItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-tasks');

        if ($this->user->isAcl(PermissionsEnum::TASKS_ADD)) {
            $menu->add($this->getTasksItemsAdd());
        }

        if ($this->user->isAcl(PermissionsEnum::TASKS_PROJECT)) {
            $this->addTasks($menu);
        }

        if ($this->user->isAcl(PermissionsEnum::TASKS_SECTION)) {
            $menu->add($this->getTasksItemsArchive());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getTasksItemsAdd(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Создать', route('task.create'));

        return $menu;
    }

    /**
     * @param NavItem $menu
     */
    private function addTasks(NavItem $menu): void
    {
        $items = $this->user->getAssociatedProjects();

        /** @var TaskProject $projectItem */
        foreach ($items as $projectItem) {
            $menu->add($this->getTasksItems($projectItem));
        }
    }

    /**
     * @param TaskProject $projectItem
     * @return MenuComposite
     */
    private function getTasksItems(TaskProject $projectItem): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('#' . $projectItem->title, route('projects.show', $projectItem->id));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getTasksItemsArchive(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Архив', route('task.archive.index'));

        return $menu;
    }
}