<?php

namespace App\Services\Menu\MenuItems;

use App\Services\FilterService;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class BaseInfoItem implements MenuItem
{
    /**
     * @var FilterService
     */
    private $filterService;

    public function __construct(FilterService $filterService)
    {
        $this->filterService = $filterService;
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-info');

        $menu->add($this->getBaseInfoItemsDocumentation());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getBaseInfoItemsDocumentation(): MenuComposite
    {
        $menu = new NavItem();
        $confluenceUrl = $this->filterService->getConfluenceUrl();

        $menu->fill('Документация', $confluenceUrl);

        return $menu;
    }
}