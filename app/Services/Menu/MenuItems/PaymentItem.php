<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class PaymentItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-payments');

        if ($this->user->isAcl(PermissionsEnum::PAYMENTS_TAB_PAYOUT_BANK)) {
            $menu->add($this->getPaymentsItemsBankAll());
        }

        if ($this->user->isAcl(PermissionsEnum::PAYMENTS_TAB_PAYOUT_CASH)) {
            $menu->add($this->getPaymentsItemsCashAll());
        }

        if ($this->user->isAcl(PermissionsEnum::PAYMENTS_TAB_ORDER_INVOICE)) {
            $menu->add($this->getPaymentsItemsOrderAll());
        }

        if ($this->user->isAcl(PermissionsEnum::PAYMENTS_TAB_ORDER_INVOICE_PAYMENTS)) {
            $menu->add($this->getPaymentsItemsOrderPayments());
        }

        if ($this->user->isAcl(PermissionsEnum::PAYMENTS_TAB_HANDLE_UPD)) {
            $menu->add($this->getPaymentsItemsOrderHandle());
        }

        if ($this->user->isAcl(PermissionsEnum::BANK_MASS_PAYMENT)) {
            $menu->add($this->getPaymentsItemsBankMass());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getPaymentsItemsBankAll(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Затраты по счёту', route('payout-bank.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getPaymentsItemsCashAll(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Наличные траты', route('payout-cash.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getPaymentsItemsOrderAll(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Проведение оплат', route('order-invoice.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getPaymentsItemsOrderPayments(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Проведенные оплаты', route('order-invoice.payments'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getPaymentsItemsOrderHandle(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Работа с УПД', route('order-invoice.handle-upd'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getPaymentsItemsBankMass(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Отчеты о массовых проведениях', route('bank-mass-payments.index'));

        return $menu;
    }
}