<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Model\OrderStatusType;
use App\Services\FilterService;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class LeadItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    /**
     * @var FilterService
     */
    private $filterService;

    public function __construct(FilterService $filterService)
    {
        $this->user = \Auth::user();
        $this->filterService = $filterService;
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-leads');

        $menu->add($this->getLeadsItemsMyPaid());
        if ($this->user->isAcl(PermissionsEnum::LEADS_ADD)) {
            $menu->add($this->getLeadsItemsAdd());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getLeadsItemsMyPaid(): MenuComposite
    {
        $menu = new NavItem();
        $leadFilter = $this->filterService->leadFilter();
        $menu->setIcon('lead-filter');
        $menu->fill('Мои оплаченные', route('order-board.filter', $leadFilter));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getLeadsItemsAdd(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Создать', route('order.create', ['type' => OrderStatusType::LEAD_TYPE]));

        return $menu;
    }
}