<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class LogisticItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-logistics');

        if ($this->user->isAcl(PermissionsEnum::DELIVERY_INTERNAL_SECTION)) {
            $menu->add($this->getLogisticsItemsInternal());
        }

        if ($this->user->isAcl(PermissionsEnum::DELIVERY_EXTERNAL_SECTION)) {
            $menu->add($this->getLogisticsItemsExternal());
        }

        if ($this->user->isAcl(PermissionsEnum::DELIVERY_EXTERNAL_CENTER_SECTION)) {
            $menu->add($this->getLogisticsItemsExternalCenter());
        }

        if ($this->user->isAcl(PermissionsEnum::DELIVERY_INTERNAL_PLAN_SECTION)) {
            $menu->add($this->getLogisticsItemsInternalPlan());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getLogisticsItemsInternal(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Внутренняя логистика', route("delivery.internal.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getLogisticsItemsExternal(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Внешняя логистика', route("delivery.external.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getLogisticsItemsExternalCenter(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Центр управления внешней логистикой', route("delivery.external_center.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getLogisticsItemsInternalPlan(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Реестр внутренних доставок', route("delivery.internal_plan.index"));

        return $menu;
    }
}