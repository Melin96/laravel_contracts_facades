<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class SettingsItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-settings');

        if ($this->user->isAcl(PermissionsEnum::GENERAL_SETTINGS)) {
            $menu->add($this->getSettingsItemsGeneral());
        }

        if ($this->user->isAcl(PermissionsEnum::COMPANY_SECTION)) {
            $menu->add($this->getSettingsItemsCompany());
        }

        if ($this->user->isAcl(PermissionsEnum::REPORTS_SECTION)) {
            $menu->add($this->getSettingsItemsReports());
            $menu->add($this->getSettingsItemsIntegrations());
            $menu->add($this->getSettingsItemsTraining());
        }

        $menu->add($this->getSettingsItemsTasks());
        $menu->add($this->getSettingsItemsOnlinePay());
        $menu->add($this->getSettingsItemsAutomate());

        if ($this->user->isAcl(PermissionsEnum::FILE_STORAGE_FOLDERS)) {
            $menu->add($this->getSettingsItemsFileStore());
        }

        $menu->add($this->getSettingsItemsUsers());
        $menu->add($this->getSettingsItemsLogistics());
        $menu->add($this->getSettingsItemsPartners());
        $menu->add($this->getSettingsItemsOrders());
        $menu->add($this->getSettingsItemsLegalEntities());
        $menu->add($this->getSettingsItemsSalePoints());
        $menu->add($this->getSettingsItemsListStores());
        $menu->add($this->getSettingsItemsLists());
        $menu->add($this->getSettingsItemsStatuses());

        if ($this->user->isAcl(PermissionsEnum::TAGS)) {
            $menu->add($this->getSettingsItemsTags());
        }

        $menu->add($this->getSettingsItemsCashier());

        if ($this->user->isAcl(PermissionsEnum::REGISTER_OF_OPERATIONS)) {
            $menu->add($this->getSettingsItemsOperations());
        }
        $menu->add($this->getSettingsItemsCalculators());
        $menu->add($this->getSettingsItemsSupport());
        $menu->add($this->getSettingsItemsPayments());
        $menu->add($this->getSettingsItemsLoyaltySystem());
        $menu->add($this->getSettingsItemsNetworks());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsGeneral(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-general');
        $menu->fill(
            'Общие настройки',
            null,
            'submenu-item-collapse submenu-item collapsed'
        );

        if ($this->user->isAcl(PermissionsEnum::GENERAL_SETTINGS_TIMEZONE)) {
            $menu->add($this->getSettingsItemsTimeZone());
        }

        if ($this->user->isAcl(PermissionsEnum::ORDER_PLANNING_SETTINGS)) {
            $menu->add($this->getSettingsItemsPlanning());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCompany(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-companies');
        $menu->fill(
            'Компании',
            route("company.index"),
            'submenu-item-collapse submenu-item collapsed',
        );

        if ($this->user->isAcl(PermissionsEnum::COMPANY_SECTION)) {
            $menu->add($this->getSettingsItemsDepartment());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsReports(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Отчеты', route('report_construct.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsIntegrations(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-integrations');
        $menu->fill(
            'Интеграции',
            route('integration.index'),
            'submenu-item-collapse submenu-item collapsed',
        );

        if ($this->user->isAcl(PermissionsEnum::MOY_SKLAD)) {
            $menu->add($this->getSettingsItemsIntegrationsMoySklad());
        }

        if ($this->user->isAcl(PermissionsEnum::CONFLUENCE_SETTINGS)) {
            $menu->add($this->getSettingsItemsIntegrationsConfluence());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTraining(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Обучение', route('training.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTasks(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-tasks');
        $menu->fill(
            'Задачи',
            route('integration.index'),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsProjects());
        $menu->add($this->getSettingsItemsProjectStatus());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsOnlinePay(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Онлайн оплаты', route("payment-systems.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsAutomate(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-automate');
        $menu->fill(
            'Автоматизация',
            route('automate.index'),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsAutomateNote());
        $menu->add($this->getSettingsItemsAutomateCond());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsFileStore(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-file_cache');
        $menu->fill(
            'Файловое хранилище',
            route('file-storage-folders.index'),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsFileStoreFolder());
        $menu->add($this->getSettingsItemsFileStoreMain());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsUsers(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-users');
        $menu->fill(
            'Пользователи',
            route("users.index"),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsUsersTemplate());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsLogistics(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-logistics');
        $menu->fill(
            'Логистика',
            null,
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsLogisticsInternal());
        $menu->add($this->getSettingsItemsLogisticsExternal());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPartners(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-partners');
        $menu->fill(
            'Контрагенты',
            route('partner.index'),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsPartnersAll());
        $menu->add($this->getSettingsItemsPartnersAd());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsOrders(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-orders');
        $menu->fill(
            'Заявки / заказы',
            route('payout-bank.index'),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsOrdersDesc());
        $menu->add($this->getSettingsItemsOrdersStage());
        $menu->add($this->getSettingsItemsOrdersDel());
        $menu->add($this->getSettingsItemsOrdersComment());
        $menu->add($this->getSettingsItemsOrdersProposal());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsLegalEntities(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-legal_entities');
        $menu->fill(
            'Юридические лица',
            route("company_legal_entities.index"),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsLegalEntitiesType());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsSalePoints(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Точки продаж', route('sale-points.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListStores(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Список складов', route('storehouses.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsLists(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-list');
        $menu->fill(
            'Списки',
            null,
            'submenu-item-collapse submenu-item collapsed',
        );

        if ($this->user->isAcl(PermissionsEnum::NOMENCLATURE_SETTINGS)) {
            $menu->add($this->getSettingsItemsListsNomenclature());
        }

        $menu->add($this->getSettingsItemsListsRefusing());
        $menu->add($this->getSettingsItemsListsCharacter());
        $menu->add($this->getSettingsItemsListsFeature());
        $menu->add($this->getSettingsItemsListsUnits());
        $menu->add($this->getSettingsItemsListsTaxes());
        $menu->add($this->getSettingsItemsListsCurrencies());
        $menu->add($this->getSettingsItemsListsEquipment());
        $menu->add($this->getSettingsItemsListsIpAdd());
        $menu->add($this->getSettingsItemsListsTechArea());
        $menu->add($this->getSettingsItemsListsDelType());
        $menu->add($this->getSettingsItemsListsDelDoc());
        $menu->add($this->getSettingsItemsListsProduct());
        $menu->add($this->getSettingsItemsListsPrints());
        $menu->add($this->getSettingsItemsListsReasons());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatuses(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-status');
        $menu->fill(
            'Статусы',
            '/statuses',
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsStatusesOrder());
        $menu->add($this->getSettingsItemsStatusesAccept());
        $menu->add($this->getSettingsItemsStatusesMove());
        $menu->add($this->getSettingsItemsStatusesInventory());
        $menu->add($this->getSettingsItemsStatusesWriteOff());
        $menu->add($this->getSettingsItemsStatusesPosting());
        $menu->add($this->getSettingsItemsStatusesInternal());
        $menu->add($this->getSettingsItemsStatusesExternal());
        $menu->add($this->getSettingsItemsStatusesInternalOrder());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTags(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-tags');
        $menu->fill(
            'Тэги',
            '/tags',
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsTagsCompany());
        $menu->add($this->getSettingsItemsTagsContractor());
        $menu->add($this->getSettingsItemsTagsOrderDef());
        $menu->add($this->getSettingsItemsTagsOrderCan());
        $menu->add($this->getSettingsItemsTagsPartner());
        $menu->add($this->getSettingsItemsTagsUser());
        $menu->add($this->getSettingsItemsTagsOrder());
        $menu->add($this->getSettingsItemsTagsRequest());
        $menu->add($this->getSettingsItemsTagsLead());
        $menu->add($this->getSettingsItemsTagsWriteOff());
        $menu->add($this->getSettingsItemsTagsPost());
        $menu->add($this->getSettingsItemsTagsReason());
        $menu->add($this->getSettingsItemsTagsPartnerUser());
        $menu->add($this->getSettingsItemsTagsTask());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCashier(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Настройки кассы', route('cashier-settings'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsOperations(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Реестр операций', route('operations.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCalculators(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-calculators');
        $menu->fill(
            'Калькуляторы',
            route('calculator.items.index'),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsCalcMain());
        $menu->add($this->getSettingsItemsCalcOption());
        $menu->add($this->getSettingsItemsCalcGroup());
        $menu->add($this->getSettingsItemsCalcNoData());
        $menu->add($this->getSettingsItemsCalcPrint());
        $menu->add($this->getSettingsItemsCalcPurchase());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsSupport(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-support');
        $menu->fill(
            'Поддержка HelloPrint',
            '/support',
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsSupportStatus());
        $menu->add($this->getSettingsItemsSupportTag());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPayments(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-set-payments');
        $menu->fill(
            'Управление платежами',
            null,
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsPaymentTypes());
        $menu->add($this->getSettingsItemsPaymentCounter());
        $menu->add($this->getSettingsItemsPaymentCash());

        if ($this->user->isAcl(PermissionsEnum::DEBT_SETTINGS)) {
            $menu->add($this->getSettingsItemsPaymentDebt());
        }

        if ($this->user->isAcl(PermissionsEnum::QUARTERS_SETTINGS)) {
            $menu->add($this->getSettingsItemsPaymentPeriods());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsLoyaltySystem(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Система лояльности', route('loyalty-system.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsNetworks(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Сайты компании', route('network-sites.index'));

        return $menu;
    }

    /**
     * submenu settings
     */

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTimeZone(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Часовой пояс', route('time-zone-settings.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPlanning(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Настройки планировщика', route('settings-order.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsDepartment(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-companies-departments');
        $menu->fill(
            'Отделы',
            route('department.index'),
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsDepartmentTypes());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsDepartmentTypes(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Типы отделов', route("department_type.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsIntegrationsMoySklad(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('МойСклад', route('moy-sklad.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsIntegrationsConfluence(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Настройки confluence', route('confluence-integration.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsProjects(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Проекты: настройка', route('projects.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsProjectStatus(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Проекты: статусы', route('projects.statuses.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsAutomateNote(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Уведомления', route('automate.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsAutomateCond(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Условия', route('automate.setting.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsFileStoreFolder(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Настройки папок', route('file-storage-folders.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsFileStoreMain(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Общие настройки', route('file-storage-settings.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsUsersTemplate(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Шаблоны прав', route("acl_template.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsLogisticsInternal(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Настройка внутренней логистики', route("delivery_internal_setting.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsLogisticsExternal(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Настройка внешней логистики', route("delivery_external_setting.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPartnersAll(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Источник контрагента и заказа', route("partner.source.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPartnersAd(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Источник рекламы контрагента и заказа', route("partner.ad.source.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsOrdersDesc(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Шаблоны описания', action('TemplateController@index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsOrdersStage(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Шаблоны маршрутов', action('StageTemplateController@index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsOrdersDel(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Типы доставки', route('order_delivery_type.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsOrdersComment(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Комментарий к оплате', route('order_comment.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsOrdersProposal(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('КП', route('proposal.settings'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsLegalEntitiesType(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Типы юридических лиц', route("partner.company.type.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsNomenclature(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Готовые номенклатуры услуг для бухгалтерии', route('nomenclatures.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsRefusing(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Отказы', route('refusing.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsCharacter(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Характеристики', url("characteristic"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsFeature(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Особенности', url("feature"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsUnits(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Единицы измерения', route("units.index"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsTaxes(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Ставки НДС для склада', url("taxes"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsCurrencies(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Валюты', url("currencies"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsEquipment(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Оборудование', route('equipment.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsIpAdd(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('IP адреса', route('ip-address.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsTechArea(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Технические участки', route('technical_area.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsDelType(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Типы доставки "до двери"', route('door_delivery_type.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsDelDoc(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Документы для доставки "до двери"', route('door_delivery_document.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsProduct(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Продукты для реестра', route('product_type_mark.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsPrints(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Отступы при печати этикеток', route('print-labels-settings.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsListsReasons(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Причины отмены заказа', route('reason-cancel-order.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesOrder(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы заказов', '/statuses/order-status');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesAccept(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы док. приемки', url("/statuses/acceptance_status"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesMove(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы док. пере-ния', url("/statuses/moving_status"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesInventory(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы док. инвентаризации', url("/statuses/inventory_status"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesWriteOff(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы списания', url("/statuses/writeoff_status"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesPosting(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы док. оприходования', url("/statuses/posting_status"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesInternal(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы заявок для внутренних доставок', url("/statuses/delivery_internal_status"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesExternal(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы заявок для курьерских доставок', url("/statuses/delivery_external_status"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsStatusesInternalOrder(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы доставки заказов для внутренней логистики', url("/statuses/delivery_internal_order_status"));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsCompany(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги компаний', route('company_tag.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsContractor(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги подрядчиков', route('settings.tags.contractor.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsOrderDef(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги брака заказ', route('settings.tags.order-defect.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsOrderCan(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги аннулирования заказа', route('settings.tags.order-cancelled.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsPartner(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги контрагентов', route('partner-tag.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsUser(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги посредников контрагентов', route('partner_user_tag.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsOrder(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги заказов', '/tags/order');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsRequest(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги заявок', '/tags/request');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsLead(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги лидов', '/tags/lead');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsWriteOff(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги причин документов списания', '/tags/writeoff');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsPost(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги причин документов оприходывания', '/tags/posting');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsReason(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги причин возврата', '/tags/CashierReturn');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsPartnerUser(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги посредников контрагентов', '/tags/PartnerUser');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsTagsTask(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги задач', '/tags/Task');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCalcMain(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Калькуляторы', route('calculator.items.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCalcOption(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Опции', route('calculator.options.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCalcGroup(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Конструктор групп', route('calculator.groups.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCalcNoData(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Нет данных', route('calculator.config.transferingorder.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCalcPrint(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Справочник печатных листов', route('calculator.printed-sheet.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsCalcPurchase(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Справочник покупных листов', route('calculator.purchase-sheet.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsSupportStatus(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы', route('support-status.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsSupportTag(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Тэги', '/tags/support');

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPaymentTypes(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статьи затрат', route('payout-type.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPaymentCounter(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-payments-counter');
        $menu->fill(
            'Затраты по счёту',
            null,
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsPaymentStatus());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPaymentStatus(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы', route('payout-bank-status.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPaymentCash(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-payments-cash');
        $menu->fill(
            'Наличные траты',
            null,
            'submenu-item-collapse submenu-item collapsed',
        );

        $menu->add($this->getSettingsItemsPaymentCashStatus());

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPaymentCashStatus(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Статусы', route('payout-cash-status.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPaymentDebt(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Настройки задолженности', route('settings-debt.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getSettingsItemsPaymentPeriods(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Учетные периоды при работе с закрывающими документами', route('quarters.index'));

        return $menu;
    }
}