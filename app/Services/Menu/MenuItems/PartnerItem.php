<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class PartnerItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-partners');

        $menu->add($this->getPartnersItemsPartners());

        if ($this->user->isAcl(PermissionsEnum::PARTNER_ACCESS_TO_CONTRACTS)) {
            $menu->add($this->getPartnersItemsContracts());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getPartnersItemsPartners(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Контрагенты', route('partner.index'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getPartnersItemsContracts(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Договоры', route('contracts.index'));

        return $menu;
    }
}