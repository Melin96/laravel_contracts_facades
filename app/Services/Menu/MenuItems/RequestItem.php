<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Model\OrderStatusType;
use App\Services\FilterService;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class RequestItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    /**
     * @var FilterService
     */
    private $filterService;

    public function __construct(FilterService $filterService)
    {
        $this->user = \Auth::user();
        $this->filterService = $filterService;
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-requests');

        $menu->add($this->getRequestsItemsMyPaid());
        if ($this->user->isAcl(PermissionsEnum::REQUESTS_ADD)) {
            $menu->add($this->getRequestsItemsAdd());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getRequestsItemsMyPaid(): MenuComposite
    {
        $menu = new NavItem();
        $requestFilter = $this->filterService->requestFilter();
        $menu->setIcon('request-filter');
        $menu->fill('Мои оплаченные', route('order-board.filter', $requestFilter));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getRequestsItemsAdd(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Создать', route('order.create', ['type' => OrderStatusType::REQUEST_TYPE]));

        return $menu;
    }
}