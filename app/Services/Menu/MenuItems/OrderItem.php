<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Model\OrderStatusType;
use App\Services\FilterService;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class OrderItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    /**
     * @var FilterService
     */
    private $filterService;

    public function __construct(FilterService $filterService)
    {
        $this->user = \Auth::user();
        $this->filterService = $filterService;
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-orders');

        $menu->add($this->getOrdersItemsMyPaid());
        $menu->add($this->getOrdersItemsMePerformer());

        if ($this->user->isAcl(PermissionsEnum::ORDER_NEW)) {
            $menu->add($this->getOrdersItemsAdd());
        }

        if ($this->user->isAcl(PermissionsEnum::ORDER_BOARD_WITH_STATUSES)) {
            $menu->add($this->getOrdersItemsBoardStatuses());
        }

        if ($this->user->isAcl(PermissionsEnum::ORDER_BOARD_ORDER_AREAS)) {
            $menu->add($this->getOrdersItemsBoardAreas());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getOrdersItemsMyPaid(): MenuComposite
    {
        $menu = new NavItem();
        $orderFilter = $this->filterService->orderFilter();
        $menu->setIcon('order-filter');
        $menu->fill('Мои оплаченные', route('order.filter', $orderFilter));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getOrdersItemsMePerformer(): MenuComposite
    {
        $menu = new NavItem();
        $orderFilterAxe = $this->filterService->orderFilterAxe();
        $menu->setIcon('order-filter');
        $menu->fill('Я исполнитель', route('order.filter', $orderFilterAxe));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getOrdersItemsAdd(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Создать', route('order.create', ['type' => OrderStatusType::ORDER_TYPE]));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getOrdersItemsBoardStatuses(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Доска по статусам', route('order-board.index', ['type' => OrderStatusType::ORDER_TYPE]));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getOrdersItemsBoardAreas(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Доска по участкам', route('order-board.order-area'));

        return $menu;
    }
}