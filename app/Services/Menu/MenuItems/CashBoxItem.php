<?php

namespace App\Services\Menu\MenuItems;

use App\Enums\Auth\PermissionsEnum;
use App\Services\Menu\Contracts\MenuComposite;
use App\Services\Menu\Contracts\MenuItem;
use App\Services\Menu\NavItem;

class CashBoxItem implements MenuItem
{
    /**
     * Current user
     * @var \App\User
     */
    private $user;

    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite
    {
        $menu = new NavItem();
        $menu->setId('submenu-mob-cashbox');

        if ($this->user->isAcl(PermissionsEnum::CASHIER_SECTION)) {
            $menu->add($this->getCashBoxItemsAdd());
        }

        if ($this->user->isAcl(PermissionsEnum::CASHIER_SESSION_FISCAL_REGISTRAR)) {
            $menu->add($this->getCashBoxItemsFiscalRegister());
        }

        if ($this->user->isAcl(PermissionsEnum::CASHIER_ANALYTICS)) {
            $menu->add($this->getCashBoxItemsAnalytics());
        }

        if ($this->user->isAcl(PermissionsEnum::CASHIER_SETTINGS)) {
            $menu->add($this->getCashBoxItemsSettings());
        }

        if ($this->user->isAcl(PermissionsEnum::CASHIER_FISCAL_REGISTRARS)) {
            $menu->add($this->getCashBoxItemsFiscal());
        }

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getCashBoxItemsAdd(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Рабочее место оператора', route('cashier.create'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getCashBoxItemsFiscalRegister(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Смена (ФР)', route('cashier-session.fiscal.registrar'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getCashBoxItemsAnalytics(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Аналитика', route('cashier-analytics.show'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getCashBoxItemsSettings(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Настройки товаров', route('cashier-settings'));

        return $menu;
    }

    /**
     * @return MenuComposite
     */
    private function getCashBoxItemsFiscal(): MenuComposite
    {
        $menu = new NavItem();
        $menu->fill('Справочник ФР', route('cashier-fiscal-registrars.index'));

        return $menu;
    }
}