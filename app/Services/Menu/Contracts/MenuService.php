<?php

namespace App\Services\Menu\Contracts;

/**
 * Handles materials of menu items.
 */
interface MenuService
{
    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite;
}
