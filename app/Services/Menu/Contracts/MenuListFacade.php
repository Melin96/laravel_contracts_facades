<?php

namespace App\Services\Menu\Contracts;

/**
 * Menu List.
 */
interface MenuListFacade
{
    /**
     * @return MenuItem
     */
    public function getLeads(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getRequests(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getOrders(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getPartners(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getTasks(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getCashBox(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getPayments(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getLogistics(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getBaseInfo(): MenuItem;

    /**
     * @return MenuItem
     */
    public function getSettings(): MenuItem;
}