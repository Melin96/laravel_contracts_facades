<?php

namespace App\Services\Menu\Contracts;

/**
 * Menu Composite.
 */
interface MenuComposite extends \IteratorAggregate, \Countable
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @return string|null
     */
    public function getUrl(): ?string;

    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @return string|null
     */
    public function getIcon(): ?string;

    /**
     * @return string|null
     */
    public function getClassName(): ?string;

    /**
     * @return MenuComposite[]
     */
    public function getIterator(): iterable;

    /**
     * @return int
     */
    public function count(): int;
}