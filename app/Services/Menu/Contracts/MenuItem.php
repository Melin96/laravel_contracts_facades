<?php

namespace App\Services\Menu\Contracts;

/**
 * Handles materials of menu items.
 */
interface MenuItem
{
    /**
     * @return MenuComposite
     */
    public function getMenu(): MenuComposite;
}
