<?php

namespace App\Services\Menu;

use App\Services\Menu\Contracts\MenuComposite;
use SplObjectStorage;

class NavItem implements MenuComposite
{
    /**
     * Name of submenu item.
     * @var string
     */
    protected $name;

    /**
     * ID of submenu item.
     * @var string
     */
    protected $id;

    /**
     * ID for icon submenu item.
     * @var string
     */
    protected $icon;

    /**
     * URL of submenu item.
     * @var string
     */
    protected $url;

    /**
     * Class name of submenu item.
     * @var string
     */
    protected $className;

    /**
     * Tree items of submenu item.
     * @var SplObjectStorage|MenuComposite[]
     */
    protected $items;

    public function __construct()
    {
        $this->items = new SplObjectStorage();
    }

    /**
     * @param string $name
     * @param string|null $url
     * @param string|null $className
     */
    public function fill(string $name, string $url = null, string $className = null): void
    {
        $this->name = $name;
        $this->url = $url;
        $this->className = $className;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return void
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return void
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return void
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return null|string
     */
    public function getClassName(): ?string
    {
        return $this->className;
    }

    /**
     * @param string $className
     * @return void
     */
    public function setClassName(string $className): void
    {
        $this->className = $className;
    }

    /**
     * @param MenuComposite $menuComposite
     */
    public function add(MenuComposite $menuComposite): void
    {
        $this->items->attach($menuComposite);
    }

    /**
     * @return iterable|MenuComposite[]
     */
    public function getIterator(): iterable
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->items->count();
    }
}